// ==UserScript==
// @name         [Rabb.it] 𝑔𝑖𝑣𝑒 𝑑𝑒𝑚 𝑒𝑚𝑜𝑡𝑒𝑠
// @namespace    gde.netux.site:rabbit
// @version      1.13.2
// @description  g̶i̶v̶e̶s̶ ̶y̶o̶u̶ ̶e̶m̶o̶t̶e̶s̶ adds emotes from TwitchTV, BTTV, TastyCat, RCS/Radiant, FrankerFaceZ and other custom emotes to rabb.it
// @author       Netux
// @contact		 Dubtrack → netux | GitHub → Netox005
// @run-at		 document-body
// @include      http://*.rabb.it/*
// @include      https://*.rabb.it/*
// @grant        none
// ==/UserScript==

(function() {
  'use strict'

  var scriptInfo = typeof GM_info === 'undefined' ? { version: '1.13.2' } : GM_info.script

  function GiveDemEmotes() {
    var emoteList = {
        twitchtv: false,
        bettertwitchtv: false,
        tastycat: false,
        rcs: false,
        frankerfacez: false,
        deviantart: false,
        redub: false
      },
      emoteElementStr = {
        set: function(innerHTML, invert) {
          var openStr = '<span class="redub emote-set">',
            closeStr = '</span>'
          return (invert ? closeStr : openStr) + (innerHTML || '') + (invert ? openStr : closeStr)
        },
        loading: function(innerHTML) {
          return '<span' +
            ' class="redub loading-emote"' +
            ' data-redub-emoteconvnum="' + emoteConversionsCount + '"' +
            '>' + innerHTML + '</span>'
        },
        error: function(emoteReplaceData) {
          return '<span' +
            ' class="redub error-emote"' +
            ' title="Failed to load emote image"' +
            '>:' + getEmoteCodeDisplayStr(emoteReplaceData) + ':</span>'
        },
        emote: function(emoteReplaceData, classes, imageSrc) {
          classes.unshift('redub', 'emote')
          return '<img' +
            ' class="' + classes.join(' ') + '"' +
            ' title=":' + getEmoteCodeDisplayStr(emoteReplaceData) + ':"' +
            ' src="' + imageSrc + '"' +
            ' data-group="' + emoteReplaceData.group + '"' +
            ' data-regexp="' + (emoteReplaceData.object.originalRegexp || emoteReplaceData.code) + '"' +
            ' />'
        },
        color: function(emoteReplaceData) {
          return '<div' +
            ' class="redub emote color"' +
            ' style="background-color: #' + emoteReplaceData.code + ';"' +
            ' title=":' + getEmoteCodeDisplayStr(emoteReplaceData) + ':"' +
            ' data-group="' + emoteReplaceData.group + '"' +
            ' data-regexp="' + emoteReplaceData.code + '"' +
            ' ></div>'
        }
      },
      emoteConversionsCount = 0,
      groupRegexp = {
        'twitchtv|ttv|tv': 'twitchtv',
        'bettertwitchtv|betterttv|bttv|btv': 'bettertwitchtv',
        'tastycat|tc|tastyplug|tp|tasty|t': 'tastycat',
        'frankerfacez|frfz|ffz|ff': 'frankerfacez',
        'rcs|radiantdj|radiant|rdj': 'rcs',
        'deviantart|deviant|da': 'deviantart',
        'redub|rdb|rdub': 'redub'
      },
      links = {
        twitchtv_api_endpoint: 'https://api.twitch.tv/kraken/chat/emoticon_images?client_id=8wp4lgj9udvwerx7xb1kqz8yci0p7p0',
        twitchtv_image_endpoint: 'https://static-cdn.jtvnw.net/emoticons/v1/%id%/%size%.0',

        bettertwitchtv_api_endpoint: 'https://api.betterttv.net/2/emotes',
        bettertwitchtv_image_endpoint: 'https://cdn.betterttv.net/emote/%id%/%size%x',

        tastycat_api_endpoint: 'https://emotes.tastycat.org/emotes.json',
        // tastycat_images_endpoint: {using object variable}

        frankerfacez_api_endpoint: 'https://rawgit.com/Jiiks/BetterDiscordApp/master/data/emotedata_ffz.json',	// Jiiks's BetterDiscord host
        frankerfacez_image_endpoint: 'https://cdn.frankerfacez.com/emoticon/%id%/%size%',

        rcs_api_endpoint: 'https://code.radiant.dj/require/emotes/radiant_emotes.json',
        rcs_image_endpoint: 'https://cdn.radiant.dj/rcs/emotes/img/%id%',

        // deviantart_api_endpoint: {none, using redub's api endpoint}
        deviantart_image_endpoint: 'https://%subdomain%.deviantart.net/%path%',

        redub_api_endpoint: 'https://github.netux.site/Dubtrack/userscript/redub/emotes_v2.json?v=' + Math.random(),
        redub_image_endpoint: 'https://github.netux.site/Dubtrack/userscript/redub/%path%',


        zero_width_api_endpoint: 'https://github.netux.site/Dubtrack/userscript/redub/zerowidth_emotes.json?v=' + Math.random()
      }
    var collectionViewEl = null

    function getEmoteCodeDisplayStr(emoteData) {
      return (emoteData.invert ? '!' : '') +
        (emoteData.group ? emoteData.group + '>' : '') +
        (emoteData.object ?
          (emoteData.object.originalRegexp ? emoteData.object.originalRegexp : emoteData.code) +
          (emoteData.object.repetitionIndex ? '-' + emoteData.object.repetitionIndex : '')
          : emoteData.code)
    }

    /* Render */
    {
      var styleVariables = {
        EMOTE_VERTICAL_SIZE: '1.875em',
        EMOTE_HORIZONTAL_SIZE: '7.5em',
        EMOTE_HAT_OFFSET: '-.35em',
        EMOTE_ERROR_UNDERLINE_SRC: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAYAAAC09K7GAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuOWwzfk4AAAAcSURBVBhXY/j//z8CMwARiIBxQDRcBkz//88AADkkH+H0w7XRAAAAAElFTkSuQmCC'
      }

      var styleEl = document.createElement('style')
      styleEl.id = 'redub-stylesheet-emotesonly'
      styleEl.innerHTML = [
        /* Loading */ '@keyframes loading-emote {',
        '	0% { opacity: 1; }',
        '	100% { opacity: .5; }',
        '}',
        '.redub.loading-emote {',
        '	color: #8C2;',
        ' 	height: $(EMOTE_VERTICAL_SIZE);',
        ' 	line-height: $(EMOTE_VERTICAL_SIZE);',
        '	vertical-align: bottom;',
        '	vertical-align: text-bottom;',
        ' 	display: inline-block;',
        '	animation: loading-emote 1s linear infinite alternate;',
        '}',
        /*  Error   */ '.redub.emote-set > .error-emote {',
        ' 	position: relative;',
        '}',
        '.redub.emote-set > .error-emote::after {',
        '    position: absolute;',
        '    content: "";',
        '    width: 100%;',
        '    height: 3px;',
        '    left: 0;',
        '	bottom: -1px;',
        ' 	background-image: url("$(EMOTE_ERROR_UNDERLINE_SRC)");',
        '	background-size: 5px;',
        '}',
        /*  Image   */ '.redub.emote-set {',
        '	position: relative;',
        '	vertical-align: bottom;',
        '	vertical-align: text-bottom;',
        '	display: inline-block;',
        '}',
        '.redub.emote-set > .emote {',
        '	width: auto !important;',
        '	height: auto  !important;',
        ' 	max-width: $(EMOTE_HORIZONTAL_SIZE) !important;',
        ' 	max-height: $(EMOTE_VERTICAL_SIZE) !important;',
        '	vertical-align: bottom;',
        '	vertical-align: text-bottom;',
        '}',
        /*  Image   */ '.redub.emote-set > .emote.inverted {',
        /* Inverted */ '	transform: rotateY(180deg);',
        '	-webkit-filter: blur(0);',
        '			filter: blur(0);',
        '}',
        /*  Image   */ '.redub.emote-set > .emote.zero-width {',
        /*zero-width*/ '	position: absolute;',
        '	left: 50%;',
        '	transform: translateX(-50%);',
        '}',
        '.redub.emote-set > .emote.zero-width.hat {',
        '	top: $(EMOTE_HAT_OFFSET);',
        '}',
        /*  Image   */ '.redub.emote-set > .emote.color {',
        /*  Color   */ ' 	width: $(EMOTE_VERTICAL_SIZE) !important;',
        ' 	height: $(EMOTE_VERTICAL_SIZE) !important;',
        '}'
      ].join('\n').replace(/\$\(([a-z0-9$_]+)\)/gi, function(all, variableName) {
        return styleVariables[variableName]
      })

      document.getElementsByTagName('head')[0].appendChild(styleEl)
    }

    var loadUtils = {
      requestAnyEmote: function(endpoint, callback) {
        var request = new XMLHttpRequest()
        request.open('GET', endpoint)
        request.onreadystatechange = function() {
          if(request.readyState !== 4)
            return
          if(request.status !== 200)
            return callback(false)

          var response = false
          try {
            response = JSON.parse(request.responseText)
          }
          catch(x) {
            console.error(x)
          }
          callback(response)
        }
        request.send()
      },
      allLoaded: false,
      loadFunctions: [
        /* TwitchTV */ function(callback) {
          var twitchtv = {}
          loadUtils.requestAnyEmote(
            links.twitchtv_api_endpoint,
            function(parsedData) {
              if(parsedData === false)
                return callback(false)

              /*
                parsedData: {
                  emoticons: [
                    {
                      id: {Number:id},
                      code: {String:regexp},
                      emoticon_set: {Number:set. not used}
                    },
                    <...>
                  ]
                }
              */
              // Sorting by ID, primarely to keep the Global Emotes (which happen to be the ones with the lower IDs) from having a repeated emote code.
              parsedData.emoticons = parsedData.emoticons.sort(function(reqEmote1, reqEmote2) {
                return reqEmote1.id - reqEmote2.id
              })

              parsedData.emoticons.forEach(function(reqEmote) {
                reqEmote.codeLc = reqEmote.code.toLowerCase()

                var emoteRegexp = reqEmote.codeLc
                for(var possibleIndex = 1; twitchtv[emoteRegexp]; possibleIndex++)
                  emoteRegexp = reqEmote.codeLc + '-' + possibleIndex;

                var emote = { id: reqEmote.id }
                {
                  if(possibleIndex - 1)
                    emote.repetitionIndex = possibleIndex - 1
                  if(reqEmote.code !== reqEmote.codeLc || possibleIndex > 0)
                    emote.originalRegexp = reqEmote.code
                  emoteRegexp = (function() { // special treatment to TwitchTV Robot, Turbo and Monkey "emojis"
                    emote.onlyIfGroupSpecified = true
                    // since TwitchTV works by ids, it's less probable they will change... If so I'll make sure to update them ;)
                    switch(reqEmote.id) {
                      /*  Robot |  Turbo  | Monkey  |      Result     */
                      case  1:
                      case 440:
                      case 499:
                        return 'happy'
                      case  2:
                      case 434:
                      case 489:
                        return 'cry'
                      case  3:
                      case 443:
                      case 496:
                        return 'grin'
                      case  4:
                      case 432:
                      case 498:
                        return 'angry'
                      case  5:
                      case 444: /* N/A */
                        return 'sleepy'
                      case  6:
                      case 437:
                      case 497:
                        return 'confused'
                      case  7:
                      case 441:
                      case 500:
                        return 'cool'
                      case  8:
                      case 436:
                      case 492:
                        return 'surprised'
                      case  9:
                      case 445:
                      case 483:
                        return 'heart'
                      case 10:
                      case 443:
                      case 493:
                        return 'unsure'
                      case 11:
                      case 439:
                      case 501:
                        return 'wink'
                      case 12:
                      case 438:
                      case 490:
                        return 'tongue'
                      case 13:
                      case 442:
                      case 491:
                        return 'twink'
                      case 14:
                      case 435: /* N/A */
                        return 'pirate'
                      /*    NOT/AV    */
                      case 484:
                        return 'hat'
                      /*    NOT/AV    */
                      case 485:
                        return 'blind'
                      /*    NOT/AV    */
                      case 486:
                        return 'love'
                      /*    NOT/AV    */
                      case 487:
                        return 'grounded'
                      /*    NOT/AV    */
                      case 488:
                        return 'pipe'
                      /*    NOT/AV    */
                      case 494:
                        return 'neutral'
                      /*    NOT/AV    */
                      case 495:
                        return 'speechless'
                      /* ----------------------------------------------- */
                      default:
                        delete emote.onlyIfGroupSpecified
                        break
                    }
                    return undefined
                  })() || emoteRegexp
                  if(reqEmote.id >= 432 && reqEmote.id <= 445)
                    emoteRegexp = 'turbo-' + emoteRegexp
                  else if(reqEmote.id >= 483 && reqEmote.id <= 501)
                    emoteRegexp = 'monkey-' + emoteRegexp
                }
                twitchtv[emoteRegexp] = emote
              })

              emoteList.twitchtv = twitchtv
              callback(true)
            },
            'TwitchTV'
          )
        },
        /* BetterTwitchTV */ function(callback) {
          var bettertwitchtv = {}
          loadUtils.requestAnyEmote(
            links.bettertwitchtv_api_endpoint,
            function(parsedData) {
              if(parsedData === false)
                return callback(false)

              /*
                parsedData: {
                  status: {Number:request status code},
                  urlTemplate: {String:image endpoint. not used since we already have it},
                  emotes: [
                    {
                      id: {String:id},
                      code: {String:regexp},
                      channel: {String:channel. not used},
                      restrictions: {Object:emote restrictions. not used},
                      imageType: {String:image type. not used}
                    },
                    <...>
                  ]
                }
              */
              if(parsedData.status !== 200) // to my understanding shouldn't happend, but it's good to always be sure
                return callback(false)

              parsedData.emotes.forEach(function(reqEmote) {
                reqEmote.codeLc = reqEmote.code.toLowerCase()

                var emoteRegexp = reqEmote.codeLc
                for(var possibleIndex = 1; bettertwitchtv[emoteRegexp]; possibleIndex++)
                  emoteRegexp = reqEmote.codeLc + '-' + possibleIndex;

                var emote = { id: reqEmote.id }
                {
                  if(possibleIndex - 1)
                    emote.repetitionIndex = possibleIndex - 1
                  if(reqEmote.code !== reqEmote.codeLc || possibleIndex > 0)
                    emote.originalRegexp = reqEmote.code
                  emoteRegexp = emoteRegexp.replace(/^bttv/, function() { // special treatment to BetterTwitchTV "emojis"
                    emote.onlyIfGroupSpecified = true
                    return ''
                  })
                }
                bettertwitchtv[emoteRegexp] = emote
              })

              emoteList.bettertwitchtv = bettertwitchtv
              callback(true)
            },
            'BetterTwitchTV'
          )
        },
        /* TastyCat */ function(callback) {
          var tastycat = {}
          loadUtils.requestAnyEmote(
            links.tastycat_api_endpoint,
            function(parsedData) {
              if(parsedData === false)
                return callback(false)

              /*
                parsedData: {
                  emotes: {
                    {String:regexp}: {String:image url},
                    <...>
                  }
                }
              */
              Object.keys(parsedData.emotes).forEach(function(code) {
                var codeLc = code.toLowerCase()

                var emoteRegexp = codeLc
                for(var possibleIndex = 1; tastycat[emoteRegexp]; possibleIndex++)
                  emoteRegexp = codeLc + '-' + possibleIndex;

                var emote = { imageSrc: parsedData.emotes[code] }
                {
                  if(possibleIndex - 1)
                    emote.repetitionIndex = possibleIndex - 1
                  if(code !== codeLc || possibleIndex > 0)
                    emote.originalRegexp = code
                }
                tastycat[emoteRegexp] = emote
              })

              emoteList.tastycat = tastycat
              callback(true)
            },
            'TastyCat'
          )
        },
        /* RCS */ function(callback) {
          var rcs = {}
          loadUtils.requestAnyEmote(
            links.rcs_api_endpoint,
            function(parsedData) {
              if(parsedData === false)
                return callback(false)

              /*
                parsedData: {
                  {Object:type. not used}: {
                    {String:regexp}: {String:id},
                    <...>
                  },
                  <...>
                }
              */
              Object.keys(parsedData.emotes).forEach(function(type) {
                if(/^(?:betterttv|frankerfacez)$/.test(type)) {
                  return
                }
                Object.keys(parsedData.emotes[type]).forEach(function(code) {
                  var codeLc = code.toLowerCase()

                  var emoteRegexp = codeLc
                  for(var possibleIndex = 1; rcs[emoteRegexp]; possibleIndex++)
                    emoteRegexp = codeLc + '-' + possibleIndex;

                  var emote = { id: parsedData.emotes[type][code] }
                  {
                    if(possibleIndex - 1)
                      emote.repetitionIndex = possibleIndex - 1
                    if(code !== codeLc || possibleIndex > 0)
                      emote.originalRegexp = code
                  }
                  rcs[emoteRegexp] = emote
                })
              })

              emoteList.rcs = rcs
              callback(true)
            },
            'RCS/Radiant'
          )
        },
        /* FrankerFaceZ */ function(callback) {
          var frankerfacez = {}
          loadUtils.requestAnyEmote(
            links.frankerfacez_api_endpoint,
            function(parsedData) {
              if(parsedData === false)
                return callback(false)

              /*
                parsedData: {
                  {String:regexp}: {String:id},
                  {String:regexp}~{Number:index of the same regexp}: {String:id},
                  <...>
                }
              */
              Object.keys(parsedData).forEach(function(code) {
                var originalCode = code
                code = code.replace(/~\d+$/, '') // remove "index of the same regexp"

                var codeLc = code.toLowerCase()

                var emoteRegexp = codeLc
                for(var possibleIndex = 1; frankerfacez[emoteRegexp]; possibleIndex++)
                  emoteRegexp = codeLc + '-' + possibleIndex;

                var emote = { id: parsedData[originalCode] }
                {
                  if(possibleIndex - 1)
                    emote.repetitionIndex = possibleIndex - 1
                  if(code !== codeLc || possibleIndex > 0)
                    emote.originalRegexp = code
                }
                frankerfacez[emoteRegexp] = emote
              })

              emoteList.frankerfacez = frankerfacez
              callback(true)
            },
            'FrankerFaceZ'
          )
        },
        /* Redub and Extra */ function(callback) {
          loadUtils.requestAnyEmote(
            links.redub_api_endpoint,
            function(parsedData) {
              if(parsedData === false)
                return callback(false)

              /*
                parsedData: {
                  {Object:group} {
                    {String:regexp}: {String:id},
                    {String:regexps, divided by `|`}: {String:id}
                  },
                  <...>,
                  redub: {
                    {String:regexp}: {String:image url},
                    {String:regexp}: {String:image url path},
                    {String:regexps, divided by `|`}: {String:image url},
                    {String:regexps, divided by `|`}: {String:image url path}
                  },
                  deviantart: {
                    {String:regexp}: {
                      subdomain: {String:image url subdomain},
                      path: {String:image url.path}
                    }
                    {String:regexps, divided by `|`}: {
                      subdomain: {String:image url subdomain},
                      path: {String:image url path}
                    }
                  }
                }
              */
              Object.keys(parsedData).forEach(function(group) {
                var groupObj = emoteList[group] || {}
                Object.keys(parsedData[group]).forEach(function(code) {
                  var codes = code.split('|')
                  for(var codeIndex = 0; codeIndex < codes.length; codeIndex++) {
                    var codeLc = codes[codeIndex].toLowerCase()

                    var emoteRegexp = codeLc
                    for(var possibleIndex = 1; groupObj[emoteRegexp]; possibleIndex++)
                      emoteRegexp = codeLc + '-' + possibleIndex;

                    var emote
                    if(codeIndex === 0) {
                      emote = { originalRegexp: codes[codeIndex] }
                      {
                        if(possibleIndex - 1)
                          emote.repetitionIndex = possibleIndex - 1
                      }
                      {
                        switch(group) {
                          default:
                            emote.id = parsedData[group][code]
                            break
                          case 'deviantart':
                          case 'redub':
                            emote.imageSrc = parsedData[group][code]
                            break
                        }
                      }
                    } else emote = codes[0]
                    groupObj[emoteRegexp] = emote
                  }
                })
                emoteList[group] = groupObj
              })

              callback(true)
            },
            'Redub and Extra'
          )
        },
        /* Zero-Width */ function(callback) {
          loadUtils.requestAnyEmote(
            links.zero_width_api_endpoint,
            function(parsedData) {
              if(parsedData === false)
                return callback(false)

              /*
                parsedData: {
                  {Object:group} {
                    {String:regexp}: {String:id},
                    {String:regexps, divided by `|`}: {String:id},
                    {String:regexp}: {
                      id: {String:id},
                      isHat: {Boolean:emote is meant to be hat, meaning the emote should be positioned a bit higher than the others. not always present}
                    }
                  }
                }
              */
              Object.keys(parsedData).forEach(function(group) {
                var groupObj = emoteList[group] || {}
                Object.keys(parsedData[group]).forEach(function(code) {
                  var emoteRegexp = code.toLowerCase()
                  var emoteData = parsedData[group][code]
                  var emoteId = emoteData, emoteZeroWidth = true
                  if(typeof emoteData === 'object') {
                    emoteId = emoteData.id
                    emoteZeroWidth = {}
                    if(typeof emoteData.isHat !== 'undefined')
                      emoteZeroWidth.isHat = emoteData.isHat
                  }
                  var emote = groupObj[emoteRegexp]
                  if(!emote) {
                    if(emoteId === null)
                      return

                    emote = {
                      id: emoteId,
                      originalRegexp: code
                    }
                  }
                  emote.zeroWidth = emoteZeroWidth
                  groupObj[emoteRegexp] = emote
                })
                emoteList[group] = groupObj
              })

              callback(true)
            },
            'Zero-Width'
          )
        },
        /* Finish */ function(callback) {
          callback(true)

          /* convert emotes on elements that "were waiting for all emotes to fully load" */
          {
            var awaitingElList = document.querySelectorAll('span.redub.awaiting-emote-convertion')
            for(var i = 0; i < awaitingElList.length; i++) {
              var awaitingEl = awaitingElList[i]
              replaceOnElement(awaitingEl)
              awaitingEl.parentNode.innerHTML = awaitingEl.innerHTML
            }
          }
        }
      ]
    }

    /* Load emotes */
    {
      var loaded = -1;
      (function loop() {
        if(++loaded >= loadUtils.loadFunctions.length) {
          loadUtils.allLoaded = true
          return
        }

        loadUtils.loadFunctions[loaded].call(undefined, loop)
      })()
    }

    {
      var replaceIfArticle = function(el) {
        if(el.nodeName.toLowerCase() !== 'article')
          return false

        replaceOnElement(el.getElementsByClassName('messageBody')[0] || el)
        return true
      }, replaceAllArticles = function() {
        var articlesElList = collectionViewEl.getElementsByTagName('article')
        for(var i = 0; i < articlesElList.length; i++)
          replaceIfArticle(articlesElList[i]);
      }

      var trayAEl = document.querySelector('#rabbitapp .social .tray.trayA')
      collectionViewEl = trayAEl.getElementsByClassName('collectionView')[0] || null

      if(collectionViewEl !== null)
        replaceAllArticles()

      var trayAObserverCallback = function(records, observer) {
        var firstRecord = records[0]
        if(firstRecord.removedNodes.length > 0) {
          collectionViewEl = null
          collectionViewObserver.disconnect()
        } else if(firstRecord.addedNodes.length > 0) {
          if(collectionViewEl !== null)
            return

          collectionViewEl = trayAEl.getElementsByClassName('collectionView')
          if(collectionViewEl.length <= 0) {
            collectionViewEl = null
            console.error('𝑔𝑖𝑣𝑒 𝑑𝑒𝑚 𝑒𝑚𝑜𝑡𝑒𝑠: apparently there is no chat for me to replace emotes :/')
            return
          }
          collectionViewEl = collectionViewEl[0]
          collectionViewObserver.observe(collectionViewEl, { childList: true })
          replaceAllArticles()
        }
        observer.takeRecords()
      }, collectionViewObserverCallback = function(records, observer) {
        var firstRecord = records[0]

        if(!replaceIfArticle(firstRecord.target) && firstRecord.addedNodes.length > 0)
          firstRecord.addedNodes.forEach(replaceIfArticle)

        observer.takeRecords()
      }

      var trayAObserver = new MutationObserver(trayAObserverCallback),
        collectionViewObserver = new MutationObserver(collectionViewObserverCallback)

      trayAObserver.observe(trayAEl, { childList: true })
    }

    function getEmoteImage(emoteObj, group, size) {
      size = size || 2
      var template = links[group + '_image_endpoint']
      switch(group) {
        default:
          return null
        case 'twitchtv':
        case 'bettertwitchtv':
        case 'rcs':
        case 'frankerfacez':
          return template.replace('%id%', emoteObj.id).replace('%size%', size)
        case 'deviantart':
          return template.replace('%subdomain%', emoteObj.imageSrc.subdomain).replace('%path%', emoteObj.imageSrc.path)
        case 'tastycat':
          return emoteObj.imageSrc
        case 'redub':
          return emoteObj.imageSrc.indexOf('http') === 0 ? emoteObj.imageSrc : template.replace('%path%', emoteObj.imageSrc)
      }
    }

    // super unoptimized function -- but oh well ¯\_(ツ)_/¯
    function replaceOnElement(el) {
      if(!loadUtils.allLoaded) {
        el.innerHTML = '<span class="redub awaiting-emote-convertion">' + el.innerHTML + '</span>'
        return el
      }

      var finalHTML = ''
      for(var nodeIndex = 0; nodeIndex < el.childNodes.length; nodeIndex++) {
        var childNode = el.childNodes[nodeIndex]
        if(childNode.nodeType !== 3) {
          // if it's not text, skip it
          finalHTML += childNode.outerHTML
          continue
        }

        var textValue = childNode.nodeValue
        if(textValue.length <= 0)
          continue

        var toReplace = []
        textValue = textValue.replace(
          /:(?::?[^\s:]+:)+/g, // on Regular Expressions 101 → https://regex101.com/r/i7YuQL/5
          function(emoteSetMatch) {
            var emoteSetList = []
            var anyActualEmotes = false, anyEmotesWithWidth = false
            var emotesFound = emoteSetMatch.split(/:{1,2}/).slice(1, -1)
            for(var emoteIndex = 0; emoteIndex < emotesFound.length; emoteIndex++) {
              var selection = emotesFound[emoteIndex]
              selection = selection.replace('&gt;', '>')

              var invert = false
              if(selection.indexOf('!') === 0) {
                invert = true
                selection = selection.substr(1)
              }

              var group, emote
              var groupWasSpecified, avoidEmoteCheck
              {
                var match
                if((match = selection.match(/^(?:colors>#?|#)((?:[a-f0-9]{3}){1,2})$/i))) {
                  groupWasSpecified = true
                  avoidEmoteCheck = true
                  emote = match[1].toUpperCase()
                  group = 'colors'
                } else if((match = selection.match(/([^>]+)>([^>]+)/))) {
                  groupWasSpecified = true
                  avoidEmoteCheck = false
                  emote = match[2].toLowerCase()
                  group = match[1].toLowerCase()
                  var groupRegexpList = Object.keys(groupRegexp)
                  for(var i = 0; i < groupRegexpList.length; i++) {
                    var gRegexp = groupRegexpList[i]
                    if(new RegExp('^(' + gRegexp + ')$').test(group)) {
                      group = groupRegexp[gRegexp]
                      break
                    }
                  }
                } else if((match = selection.match(/([^>]+)/))) {
                  groupWasSpecified = false
                  avoidEmoteCheck = false
                  emote = match[1].toLowerCase()
                  group = false
                  var groupList = Object.keys(emoteList)
                  for(var j = 0; j < groupList.length; j++) {
                    var emoteGroup = groupList[j]
                    if(emoteList[emoteGroup][emote]) {
                      group = emoteGroup
                      break
                    }
                  }
                }

                if(!group) {
                  emoteSetList.push(selection)
                  continue
                }
              }

              var groupEmoteList = emoteList[group],
                emoteObj
              if(!avoidEmoteCheck) {
                if(groupEmoteList) {
                  do emoteObj = groupEmoteList[typeof emoteObj === 'string' ? emoteObj.toLowerCase() : emote]
                  while(typeof emoteObj === 'string')
                  if(!emoteObj) {
                    emoteSetList.push(selection)
                    continue
                  }
                } else continue
                if(emoteObj.onlyIfGroupSpecified && !groupWasSpecified) {
                  emoteSetList.push(selection)
                  continue
                }
              }

              emoteSetList.push({
                group: group,
                code: emote,
                object: emoteObj,
                invert: invert
              })

              anyActualEmotes = true
              if(emoteObj && !emoteObj.zeroWidth)
                anyEmotesWithWidth = true
            }

            if(!anyActualEmotes)
              return emoteSetMatch

            emoteConversionsCount++
            toReplace.push({
              emotes: emoteSetList,
              emoteConversionNumber: emoteConversionsCount,
              anyEmotesWithWidth: anyEmotesWithWidth
            })

            return emoteElementStr.loading(emoteSetMatch, emoteConversionsCount)
          }
        )

        if(toReplace.length > 0) {
          toReplace.forEach(function(setReplaceData) {
            var leftToLoad = setReplaceData.emotes.length,
              emoteSetInnerHTML = []

            setReplaceData.emotes.forEach(function(emoteReplaceData, emoteIndex) {
              var emoteImg
              var replaceLoadingElement = function() {
                var elToReplace = document.querySelector('span.redub.loading-emote[data-redub-emoteconvnum="' + setReplaceData.emoteConversionNumber + '"]')
                if(!elToReplace) {
                  if(replaceLoadingElement.tried)
                    return (replaceLoadingElement.tried = false)
                  replaceLoadingElement.tried = true
                  setTimeout(replaceLoadingElement, 0) // element might be injented later
                  return
                }
                elToReplace.outerHTML = emoteElementStr.set(emoteSetInnerHTML.join(''), false)
              }
              var emoteHasLoaded = function(replaceTo) {
                if(emoteImg) {
                  emoteImg.onload = undefined
                  emoteImg.onerror = undefined
                }

                leftToLoad--
                emoteSetInnerHTML[emoteIndex] = replaceTo

                if(leftToLoad <= 0)
                  replaceLoadingElement()
              }

              if(typeof emoteReplaceData === 'string')
                emoteHasLoaded(emoteElementStr.set(':' + emoteReplaceData + ':', true))
              else if(emoteReplaceData.group === 'colors')
                emoteHasLoaded(emoteElementStr.color(emoteReplaceData))
              else {
                emoteImg = new Image()
                emoteImg.onload = function() {
                  var imageClasses = []
                  {
                    if(emoteReplaceData.invert)
                      imageClasses.push('inverted')
                    if(emoteReplaceData.object.zeroWidth && setReplaceData.anyEmotesWithWidth) {
                      imageClasses.push('zero-width')
                      if(emoteReplaceData.object.zeroWidth.isHat)
                        imageClasses.push('hat')
                    }
                  }
                  emoteHasLoaded(emoteElementStr.emote(emoteReplaceData, imageClasses, emoteImg.src))
                }
                emoteImg.onerror = function() {
                  if(emoteReplaceData.group === 'frankerfacez' && !emoteReplaceData.object.useSmallScale) {
                    emoteReplaceData.object.useSmallScale = true
                    emoteImg.src = getEmoteImage(emoteReplaceData.object, emoteReplaceData.group, 1)
                    return
                  }
                  emoteHasLoaded(emoteElementStr.error(emoteReplaceData))
                }
                emoteImg.src = getEmoteImage(emoteReplaceData.object, emoteReplaceData.group, emoteReplaceData.object.useSmallScale ? 1 : 2)
              }
            })
          })
        }
        finalHTML += textValue
      }

      el.innerHTML = finalHTML
    }

    window.GiveDemEmotes = { version: scriptInfo.version }
    localStorage.setItem('gde-v', window.GiveDemEmotes.version)
  }

  var bootInterval = setInterval(function() {
    if(document.readyState !== 'complete')
      return

    if(typeof $ === 'undefined') // `jQuery` doesn't exist, it uses a variant with numbers after it (which are inconcistent), so we have to use `$`
      return

    if(document.querySelector('#rabbitapp .social .tray.trayA') === null)
      return

    clearInterval(bootInterval)

    if(typeof MutationObserver === 'undefined') {
      console.error('𝑔𝑖𝑣𝑒 𝑑𝑒𝑚 𝑒𝑚𝑜𝑡𝑒𝑠: Browser doesn\'t support MutationObserver. I can\'t replace emotes here :/')
      // TODO: Add message in chat or popup
      return
    }

    if(typeof window.GiveDemEmotes !== 'undefined') {
      console.error(
        '𝑔𝑖𝑣𝑒 𝑑𝑒𝑚 𝑒𝑚𝑜𝑡𝑒𝑠 is already initialized %c', // bingMad TwitchTV emote
        'background: url("https://static-cdn.jtvnw.net/emoticons/v1/54937/1.0") no-repeat center; background-size: 2em; padding: 1em; line-height: 2em'
      )
      // TODO: Add message in chat or make a popup
      return
    }

    new GiveDemEmotes()
  }, 500)
})()
